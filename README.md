# Restify + React Admin

## Тухай

Restify + Sequelize ORM ашиглан Оюутан, Багш, Анги, Хичээл гэсэн хичээлийн бүртгэлийн бичил системийн CRUD үйлдлүүд хийж, React-Admin-ны API resource ашиглан хийсэн CRUD үйлдлүүдэд UI зурав.

## Backend
Start хийхээс өмнө /server/config/config.js дээр mysql-н тохиргоог хийж өгнө үү.

```
cd server
npm install
npm run migrate-local
npm start
```

## Frontend
Start хийхээс өмнө /server/config/config.js дээр mysql-н тохиргоог хийж өгнө үү.

```
cd client
npm install
npm start
```
