import { Admin, Resource } from "react-admin";
import DataProvider from "./Providers/DataProvider";
import AuthProviders from "./Providers/AuthProvider";

import GroupCreate from "./Pages/Groups/GroupCreate";
import GroupEdit from "./Pages/Groups/GroupEdit";
import GroupList from "./Pages/Groups/GroupList";

import StudentList from "./Pages/Students/StudentList";
import StudentCreate from "./Pages/Students/StudentCreate";
import StudentEdit from "./Pages/Students/StudentEdit";

import TeacherList from "./Pages/Teachers/TeacherList";
import TeacherCreate from "./Pages/Teachers/TeacherCreate";
import TeacherEdit from "./Pages/Teachers/TeacherEdit";

import LessonList from "./Pages/Lesson/LessonList";
import LessonCreate from "./Pages/Lesson/LessonCreate";
import LessonEdit from "./Pages/Lesson/LessonEdit";

function App() {
    return (
        <Admin dataProvider={DataProvider} authProvider={AuthProviders}>
            <Resource name="groups" list={GroupList} create={GroupCreate} edit={GroupEdit} />
            <Resource name="students" list={StudentList} create={StudentCreate} edit={StudentEdit} />
            <Resource name="teachers" list={TeacherList} create={TeacherCreate} edit={TeacherEdit} />
            <Resource name="lessons" list={LessonList} create={LessonCreate} edit={LessonEdit} />
        </Admin>
    );
}

export default App;
