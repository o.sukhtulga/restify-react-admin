import React from "react";
import { useAuthenticated, Create, SimpleForm, TextInput } from "react-admin";

const GroupCreate = (props) => {
    useAuthenticated();
    return (
        <Create title="Register new group" {...props}>
            <SimpleForm>
                <TextInput source="name" label="Group name" />
            </SimpleForm>
        </Create>
    );
};

export default GroupCreate;
