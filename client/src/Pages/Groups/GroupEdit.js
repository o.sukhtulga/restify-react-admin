import React from "react";
import { useAuthenticated, Edit, SimpleForm, TextInput } from "react-admin";

const GroupEdit = (props) => {
    useAuthenticated();
    return (
        <Edit title="Edit Group" {...props}>
            <SimpleForm>
                <TextInput source="id" label="Group id" disabled />
                <TextInput source="name" label="Group name" />
            </SimpleForm>
        </Edit>
    );
};

export default GroupEdit;
