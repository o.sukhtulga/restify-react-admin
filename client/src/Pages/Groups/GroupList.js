import React from "react";
import { List, Datagrid, TextField, DateField, EditButton, DeleteButton, useAuthenticated } from "react-admin";

const GroupList = (props) => {
    useAuthenticated();
    return (
        <List {...props}>
            <Datagrid>
                <TextField source="id"></TextField>
                <TextField source="name"></TextField>
                <DateField source="createdAt"></DateField>
                <EditButton />
                <DeleteButton />
            </Datagrid>
        </List>
    );
};

export default GroupList;
