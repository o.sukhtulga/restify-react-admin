import React from "react";
import { useAuthenticated, Create, SimpleForm, TextInput, useGetList, Loading, NumberInput, TimeInput, SelectInput, SelectArrayInput } from "react-admin";

const LessonCreate = (props) => {
    useAuthenticated();
    const { data, isLoading } = useGetList("groups");
    const { data: teacherData, isLoading: isTeacherLoading } = useGetList("teachers");

    if (isLoading || isTeacherLoading) {
        return <Loading />;
    }

    return (
        <Create title="Register new teacher" {...props}>
            <SimpleForm>
                <TextInput source="title" label="Lesson name" />
                <NumberInput source="room" label="Room number" />
                <TimeInput source="startTime" />
                <TimeInput source="endTime" />
                <SelectArrayInput required source="groups" label="Group" choices={data.map((item) => ({ id: item.id, name: item.name }))} />
                <SelectInput required source="teacher" label="Teacher" choices={teacherData.map((item) => ({ id: item.id, name: item.name }))} />
            </SimpleForm>
        </Create>
    );
};

export default LessonCreate;
