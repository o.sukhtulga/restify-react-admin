import React, { useEffect, useState } from "react";
import {
    useAuthenticated,
    Edit,
    SimpleForm,
    TextInput,
    NumberInput,
    TimeInput,
    SelectArrayInput,
    SelectInput,
    Loading,
    useGetList,
    ReferenceArrayInput,
    ChipField,
} from "react-admin";

const LessonEdit = (props) => {
    useAuthenticated();

    const { data: groupData, isLoading: isGroupLoading } = useGetList("groups");
    const { data: teacherData, isLoading: isTeacherLoading } = useGetList("teachers");

    if (isGroupLoading || isTeacherLoading) {
        return <Loading />;
    }

    return (
        <Edit title="Edit teacher" {...props}>
            <SimpleForm>
                <TextInput source="id" label="Lesson id" disabled />
                <TextInput source="title" label="Lesson name" />
                <NumberInput source="room" label="Room number" />
                <TimeInput source="startTime" />
                <TimeInput source="endTime" />
                <SelectArrayInput required source="groups" label="Group" choices={groupData?.map((item) => ({ id: item.id, name: item.name }))} />
                <SelectInput required source="teacher" label="Teacher" choices={teacherData?.map((item) => ({ id: item.id, name: item.name }))} />
            </SimpleForm>
        </Edit>
    );
};

export default LessonEdit;
