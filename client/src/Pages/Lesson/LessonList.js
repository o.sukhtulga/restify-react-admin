import React from "react";
import {
    List,
    Datagrid,
    TextField,
    DateField,
    EditButton,
    DeleteButton,
    useAuthenticated,
    NumberField,
    ReferenceField,
    ReferenceArrayField,
    useGetMany,
    SingleFieldList,
    ChipField,
    FunctionField,
} from "react-admin";
import moment from "moment";

const LessonList = (props) => {
    useAuthenticated();

    return (
        <List {...props}>
            <Datagrid>
                <TextField source="id" />
                <TextField source="title" />
                <NumberField source="room" />
                {/* <TextField source="startTime" /> */}
                <FunctionField label="Start time" render={(record) => moment(record.startTime).format("YYYY-MM-DD HH:MM")} />
                <FunctionField label="End time" render={(record) => moment(record.endTime).format("YYYY-MM-DD HH:MM")} />
                <ReferenceArrayField source="groups" reference="groups">
                    <SingleFieldList>
                        <ChipField source="name" />
                    </SingleFieldList>
                </ReferenceArrayField>
                <ReferenceField source="teacher" reference="teachers">
                    <TextField source="name" />
                </ReferenceField>
                <DateField source="createdAt" />
                <EditButton />
                <DeleteButton />
            </Datagrid>
        </List>
    );
};

export default LessonList;
