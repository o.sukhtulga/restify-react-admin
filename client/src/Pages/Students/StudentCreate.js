import React from "react";
import { useAuthenticated, Create, SimpleForm, TextInput, SelectInput, useGetList, Loading } from "react-admin";

const StudentCreate = (props) => {
    useAuthenticated();
    const { data, isLoading } = useGetList("groups");

    if (isLoading) {
        return <Loading />;
    }

    return (
        <Create title="Register new student" {...props}>
            <SimpleForm>
                <TextInput source="name" label="Student name" required />
                <SelectInput required source="group" label="Group" choices={data.map((item) => ({ id: item.id, name: item.name }))} />
            </SimpleForm>
        </Create>
    );
};

export default StudentCreate;
