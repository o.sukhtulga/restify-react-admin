import React from "react";
import { useAuthenticated, Edit, SimpleForm, TextInput, SelectInput, useGetList, Loading } from "react-admin";

const StudentEdit = (props) => {
    useAuthenticated();
    const { data, isLoading } = useGetList("groups");

    if (isLoading) {
        return <Loading />;
    }

    return (
        <Edit title="Edit student" {...props}>
            <SimpleForm>
                <TextInput source="name" label="Student name" required />
                <SelectInput required source="group" label="Group" choices={data.map((item) => ({ id: item.id, name: item.name }))} />
            </SimpleForm>
        </Edit>
    );
};

export default StudentEdit;
