import React, { useEffect } from "react";
import { List, Datagrid, TextField, DateField, EditButton, DeleteButton, useAuthenticated, ReferenceField } from "react-admin";

const StudentList = (props) => {
    useAuthenticated();
    return (
        <List {...props}>
            <Datagrid>
                <TextField source="id"></TextField>
                <TextField source="name"></TextField>
                <ReferenceField source="group" reference="groups">
                    <TextField source="name" />
                </ReferenceField>
                <DateField source="createdAt"></DateField>
                <EditButton />
                <DeleteButton />
            </Datagrid>
        </List>
    );
};

export default StudentList;
