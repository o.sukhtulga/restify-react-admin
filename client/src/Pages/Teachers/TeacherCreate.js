import React from "react";
import { useAuthenticated, Create, SimpleForm, TextInput } from "react-admin";

const TeacherCreate = (props) => {
    useAuthenticated();
    return (
        <Create title="Register new teacher" {...props}>
            <SimpleForm>
                <TextInput source="name" label="Group name" />
            </SimpleForm>
        </Create>
    );
};

export default TeacherCreate;
