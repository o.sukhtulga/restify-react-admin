import React from "react";
import { useAuthenticated, Edit, SimpleForm, TextInput } from "react-admin";

const TeacherEdit = (props) => {
    useAuthenticated();
    return (
        <Edit title="Edit teacher" {...props}>
            <SimpleForm>
                <TextInput source="id" label="Teacher` id" disabled />
                <TextInput source="name" label="Teacher name" />
            </SimpleForm>
        </Edit>
    );
};

export default TeacherEdit;
