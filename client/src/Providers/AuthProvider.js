export default {
    login: async ({ username, password }) => {
        const request = new Request("http://localhost:8000/api/login", {
            method: "POST",
            body: JSON.stringify({ username, password }),
            headers: new Headers({ "Content-Type": "application/json", "Access-Control-Allow-Origin": "*" }),
        });
        const response = await fetch(request);
        const body = await response.json();

        if (response.status !== 200) {
            throw new Error(body.message);
        }

        localStorage.setItem("token", body.token);
    },
    checkError: (error) => Promise.resolve(),
    checkAuth: () => {
        return localStorage.getItem("token") ? Promise.resolve() : Promise.reject();
    },
    logout: () => {
        localStorage.removeItem("token");
        return Promise.resolve();
    },
    getIdentity: () => {
        /* ... */
    },
    getPermissions: () => Promise.resolve(""),
};
