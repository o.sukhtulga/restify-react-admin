import restProvider from "ra-data-simple-rest";
import { fetchUtils } from "react-admin";

const httpClient = async (url, options = {}) => {
    if (!options.headers) {
        options.headers = new Headers({ Accept: "application/json" });
    }

    const token = localStorage.getItem("token");
    options.headers.set("Authorization", `Bearer ${token}`);
    options.headers.set("Access-Control-Allow-Origin", "*");

    return fetchUtils.fetchJson(url, options);
};

export default restProvider("http://localhost:8000", httpClient);
