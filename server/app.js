require("dotenv").config();

const restify = require("restify");
const routes = require("./routes/routes");
const db = require("./models/index");
const corsMiddleware = require("restify-cors-middleware2");

const server = restify.createServer({
    name: "restify-example",
    version: "1.0.0",
});

server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser());
const cors = corsMiddleware({
    preflightMaxAge: 5, //Optional
    origins: ["*"],
    allowHeaders: ["*"],
    exposeHeaders: ["*"],
});

server.pre(cors.preflight);
server.use(cors.actual);

routes(server);

db.sequelize
    .authenticate()
    .then(() => {
        console.log("Connection has been established successfully.");
    })
    .catch((err) => {
        console.error("Unable to connect to the database:", err);
    });

server.listen(process.env.PORT, function () {
    console.log("%s listening at %s", server.name, server.url);
});
