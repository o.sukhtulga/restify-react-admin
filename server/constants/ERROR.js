module.exports = Object.freeze({
    NO_CREDENTIALS: "Хандах эрхгүй байна",

    // User
    USER_DUPLICATED: "Системийн хэрэглэгч бүртгэгдсэн байна",
    USER_NOT_FOUND: "Системийн хэрэглэгч олдсонгүй",
    USERNAME_OR_PASSWORD_INCORRECT: "Нэвтрэх нэр эсвэл нууц үг буруу байна",

    // Student
    STUDENT_DUPLICATED: "Оюутан бүртгэгдсэн байна",
    STUDENT_NOTFOUND: "Бүртгэлтэй оюутан олдсонгүй",

    // Group
    GROUP_DUPLICATED: "Анги бүртгэгдсэн байна",
    GROUP_NOTFOUND: "Бүртгэлтэй анги олдсонгүй",

    // Teacher
    TEACHER_DUPLICATED: "Багш бүртгэгдсэн байна",
    TEACHER_NOTFOUND: "Бүртгэлтэй багш олдсонгүй",

    // Lesson
    LESSON_DUPLICATED: "Хичээл бүртгэгдсэн байна",
    LESSON_NOTFOUND: "Бүртгэлтэй хичээл олдсонгүй",
});
