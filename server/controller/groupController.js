const validator = require("../utils/validator");
const logger = require("../utils/logger");
const Joi = require("joi");
const errors = require("restify-errors");

const { create, list, edit, get, remove } = require("../services/groupService");

const createGroup = async (req, res, next) => {
    try {
        //validate body data
        const schema = Joi.object({
            name: Joi.string().required(),
        });
        await validator(schema, req.body);

        const group = await create(req.body);

        res.send(200, {
            id: group.id,
            name: group.name,
        });
    } catch (error) {
        logger.error(error);
        next(new errors.BadRequestError(error.message));
    }
};

const getGroup = async (req, res, next) => {
    try {
        //validate body data
        const schema = Joi.object({
            id: Joi.string().required(),
        });
        await validator(schema, req.params);

        const group = await get(req.params);

        res.send(200, group);
    } catch (error) {
        logger.error(error);
        next(new errors.BadRequestError(error.message));
    }
};

const editGroup = async (req, res, next) => {
    try {
        //validate body data
        const schema = Joi.object({
            id: Joi.number().required(),
            name: Joi.string().required(),
            createdAt: Joi.string().optional(),
            updatedAt: Joi.string().optional(),
        });
        await validator(schema, req.body);

        const group = await edit(req.body);

        res.send(200, group);
    } catch (error) {
        logger.error(error);
        next(new errors.BadRequestError(error.message));
    }
};

const getList = async (req, res, next) => {
    try {
        const schema = Joi.object({
            filter: Joi.string().optional().allow(null),
            range: Joi.string().optional().allow(null),
            sort: Joi.string().optional().allow(null),
        });
        await validator(schema, req.query);

        const { count, rows } = await list(req.query);

        res.header("Content-Range", `groups, 0-${count}/${count}`);
        res.send(200, rows);
    } catch (error) {
        logger.error(error);
        next(new errors.BadRequestError(error.message));
    }
};

const removeGroup = async (req, res, next) => {
    try {
        //validate body data
        const schema = Joi.object({
            id: Joi.string().required(),
        });
        await validator(schema, req.params);

        await remove(req.params);

        res.send(200);
    } catch (error) {
        logger.error(error);
        next(new errors.BadRequestError(error.message));
    }
};

module.exports = { createGroup, getList, getGroup, editGroup, removeGroup };
