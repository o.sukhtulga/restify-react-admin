const validator = require("../utils/validator");
const logger = require("../utils/logger");
const Joi = require("joi");
const errors = require("restify-errors");

const { create, list, edit, get, remove } = require("../services/lessonService");

const createLesson = async (req, res, next) => {
    try {
        //validate body data
        const schema = Joi.object({
            title: Joi.string().required(),
            groups: Joi.array().required(),
            startTime: Joi.string().required(),
            endTime: Joi.string().required(),
            room: Joi.number().required(),
            teacher: Joi.number().required(),
        });
        await validator(schema, req.body);

        const lesson = await create(req.body);

        res.send(200, {
            id: lesson.id,
            title: lesson.title,
        });
    } catch (error) {
        logger.error(error);
        next(new errors.BadRequestError(error.message));
    }
};

const getList = async (req, res, next) => {
    try {
        const schema = Joi.object({
            filter: Joi.string().optional().allow(null),
            range: Joi.string().optional().allow(null),
            sort: Joi.string().optional().allow(null),
        });
        await validator(schema, req.query);

        const { count, rows } = await list(req.query);

        res.header("Content-Range", `groups, 0-${count}/${count}`);
        res.send(200, rows);
    } catch (error) {
        logger.error(error);
        next(new errors.BadRequestError(error.message));
    }
};

const getLesson = async (req, res, next) => {
    try {
        //validate body data
        const schema = Joi.object({
            id: Joi.string().required(),
        });
        await validator(schema, req.params);

        const lesson = await get(req.params);

        res.send(200, lesson);
    } catch (error) {
        logger.error(error);
        next(new errors.BadRequestError(error.message));
    }
};

const editLesson = async (req, res, next) => {
    try {
        //validate body data
        const schema = Joi.object({
            id: Joi.number().required(),
            title: Joi.string().required(),
            groups: Joi.array().required(),
            startTime: Joi.string().required(),
            endTime: Joi.string().required(),
            room: Joi.number().required(),
            teacher: Joi.number().required(),

            createdAt: Joi.string().optional(),
            updatedAt: Joi.string().optional(),
        });
        await validator(schema, req.body);

        const lesson = await edit(req.body);

        res.send(200, lesson);
    } catch (error) {
        logger.error(error);
        next(new errors.BadRequestError(error.message));
    }
};

const removeLesson = async (req, res, next) => {
    try {
        //validate body data
        const schema = Joi.object({
            id: Joi.string().required(),
        });
        await validator(schema, req.params);

        await remove(req.params);

        res.send(200);
    } catch (error) {
        logger.error(error);
        next(new errors.BadRequestError(error.message));
    }
};

module.exports = { createLesson, getList, getLesson, editLesson, removeLesson };
