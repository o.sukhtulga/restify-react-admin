const validator = require("../utils/validator");
const logger = require("../utils/logger");
const Joi = require("joi");
const errors = require("restify-errors");

const { create, list, get, edit, remove } = require("../services/studentService");

const creatStudent = async (req, res, next) => {
    try {
        //validate body data
        const schema = Joi.object({
            name: Joi.string().required(),
            group: Joi.number().required(),
        });
        await validator(schema, req.body);

        const student = await create(req.body);

        res.send(200, {
            id: student.id,
            name: student.name,
            group: student.group,
        });
    } catch (error) {
        logger.error(error);
        next(new errors.BadRequestError(error.message));
    }
};

const getList = async (req, res, next) => {
    try {
        const schema = Joi.object({
            filter: Joi.string().optional().allow(null),
            range: Joi.string().optional().allow(null),
            sort: Joi.string().optional().allow(null),
        });
        await validator(schema, req.query);

        const { count, rows } = await list(req.query);

        res.header("Content-Range", `groups, 0-${count}/${count}`);
        res.send(200, rows);
    } catch (error) {
        logger.error(error);
        next(new errors.BadRequestError(error.message));
    }
};

const getStudent = async (req, res, next) => {
    try {
        //validate body data
        const schema = Joi.object({
            id: Joi.string().required(),
        });
        await validator(schema, req.params);

        const student = await get(req.params);

        res.send(200, student);
    } catch (error) {
        logger.error(error);
        next(new errors.BadRequestError(error.message));
    }
};

const editStudent = async (req, res, next) => {
    try {
        //validate body data
        const schema = Joi.object({
            id: Joi.number().required(),
            name: Joi.string().required(),
            group: Joi.number().required(),
            createdAt: Joi.string().optional(),
            updatedAt: Joi.string().optional(),
        });
        await validator(schema, req.body);

        const student = await edit(req.body);

        res.send(200, student);
    } catch (error) {
        logger.error(error);
        next(new errors.BadRequestError(error.message));
    }
};

const removeStudent = async (req, res, next) => {
    try {
        //validate body data
        const schema = Joi.object({
            id: Joi.string().required(),
        });
        await validator(schema, req.params);

        await remove(req.params);

        res.send(200);
    } catch (error) {
        logger.error(error);
        next(new errors.BadRequestError(error.message));
    }
};

module.exports = {
    creatStudent,
    getList,
    getStudent,
    editStudent,
    removeStudent,
};
