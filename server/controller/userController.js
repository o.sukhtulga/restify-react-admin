const validator = require("../utils/validator");
const logger = require("../utils/logger");
const Joi = require("joi");
const errors = require("restify-errors");

const { createUser, loginUser } = require("../services/userService");

const register = async (req, res, next) => {
    try {
        //validate body data
        const schema = Joi.object({
            username: Joi.string().required().min(3).max(45),
            password: Joi.string().required().min(8),
        });
        await validator(schema, req.body);

        const user = await createUser(req.body);

        res.send(200, {
            username: user.username,
            userid: user.id,
        });
    } catch (error) {
        logger.error(error);
        next(new errors.BadRequestError(error.message));
    }
};

const login = async (req, res, next) => {
    try {
        //validate body data
        const schema = Joi.object({
            username: Joi.string().required(),
            password: Joi.string().required(),
        });
        await validator(schema, req.body);

        const response = await loginUser(req.body);

        res.send(200, response);
    } catch (error) {
        logger.error(error);
        next(new errors.BadRequestError(error.message));
    }
};

module.exports = {
    register,
    login,
};
