const jwt = require("jsonwebtoken");
const ERROR = require("../constants/ERROR");
const errors = require("restify-errors");

const config = process.env;

const auth = async (req, res, next) => {
    const { authorization } = req.headers;

    if (!authorization) {
        next(new errors.NotAuthorizedError(ERROR.NO_CREDENTIALS));
    }

    const isBearerToken = authorization.startsWith("Bearer ");
    if (!isBearerToken) {
        next(new errors.NotAuthorizedError(ERROR.NO_CREDENTIALS));
    }

    try {
        const token = await authorization.slice(7, authorization.length);
        const decoded = jwt.verify(token, config.TOKEN_KEY);

        req.user = decoded;

        return next();
    } catch (error) {
        next(new errors.BadRequestError(error.message));
    }
};

module.exports = auth;
