"use strict";
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable(
            "groups",
            {
                id: {
                    type: Sequelize.INTEGER,
                    primaryKey: true,
                    autoIncrement: true,
                    allowNull: false,
                },
                name: {
                    type: Sequelize.STRING(96),
                    allowNull: true,
                },
                createdAt: Sequelize.DATE,
                updatedAt: Sequelize.DATE,
            },
            {
                charset: "utf8",
                collate: "utf8_unicode_ci",
            }
        );
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable("groups");
    },
};
