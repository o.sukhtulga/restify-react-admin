"use strict";
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable(
            "lessons",
            {
                id: {
                    type: Sequelize.INTEGER,
                    primaryKey: true,
                    autoIncrement: true,
                    allowNull: false,
                },
                title: {
                    type: Sequelize.STRING(96),
                    allowNull: true,
                },
                groups: {
                    type: Sequelize.STRING(255),
                    allowNull: true,
                },
                teacher: {
                    type: Sequelize.STRING(96),
                    allowNull: true,
                },
                room: {
                    type: Sequelize.INTEGER(10),
                    allowNull: true,
                },
                startTime: {
                    type: Sequelize.STRING(96),
                    allowNull: true,
                },
                endTime: {
                    type: Sequelize.STRING(96),
                    allowNull: true,
                },
                createdAt: Sequelize.DATE,
                updatedAt: Sequelize.DATE,
            },
            {
                charset: "utf8",
                collate: "utf8_unicode_ci",
            }
        );
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable("lessons");
    },
};
