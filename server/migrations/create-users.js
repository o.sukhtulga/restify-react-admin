"use strict";
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable(
            "users",
            {
                id: {
                    type: Sequelize.INTEGER,
                    primaryKey: true,
                    autoIncrement: true,
                    allowNull: false,
                },
                username: {
                    type: Sequelize.STRING(255),
                    allowNull: true,
                },
                password: {
                    type: Sequelize.STRING(500),
                    allowNull: true,
                },
                createdAt: Sequelize.DATE,
                updatedAt: Sequelize.DATE,
            },
            {
                charset: "utf8",
                collate: "utf8_unicode_ci",
            }
        );
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable("users");
    },
};
