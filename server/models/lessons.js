"use strict";
module.exports = (sequelize, DataTypes) => {
    const Lesson = sequelize.define(
        "lessons",
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
            },
            title: {
                type: DataTypes.STRING(96),
                allowNull: true,
            },
            groups: {
                type: DataTypes.STRING(255),
                allowNull: true,
            },
            teacher: {
                type: DataTypes.STRING(96),
                allowNull: true,
            },
            room: {
                type: DataTypes.INTEGER(10),
                allowNull: true,
            },
            startTime: {
                type: DataTypes.STRING(96),
                allowNull: true,
            },
            endTime: {
                type: DataTypes.STRING(96),
                allowNull: true,
            },
            createdAt: DataTypes.DATE,
            updatedAt: DataTypes.DATE,
        },
        {
            charset: "utf8",
            collate: "utf8_unicode_ci",
        }
    );
    return Lesson;
};
