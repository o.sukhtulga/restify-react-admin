"use strict";
module.exports = (sequelize, DataTypes) => {
    const Teacher = sequelize.define(
        "teachers",
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
            },
            name: {
                type: DataTypes.STRING(96),
                allowNull: true,
            },
            createdAt: DataTypes.DATE,
            updatedAt: DataTypes.DATE,
        },
        {
            charset: "utf8",
            collate: "utf8_unicode_ci",
        }
    );
    return Teacher;
};
