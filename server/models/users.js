"use strict";
module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define(
        "users",
        {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
            },
            username: {
                type: DataTypes.STRING(45),
                allowNull: true,
            },
            password: {
                type: DataTypes.STRING(500),
                allowNull: true,
            },
            createdAt: DataTypes.DATE,
            updatedAt: DataTypes.DATE,
        },
        {
            charset: "utf8",
            collate: "utf8_unicode_ci",
        }
    );
    return User;
};
