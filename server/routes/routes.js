const userController = require("../controller/userController");
const groupController = require("../controller/groupController");
const studentController = require("../controller/studentController");
const teacherController = require("../controller/teacherController");
const lessonController = require("../controller/lessonController");
const auth = require("../middleware/auth");

module.exports = (server) => {
    // sys user routes
    server.post("/api/register", userController.register);
    server.post("/api/login", userController.login);

    // group routes
    server.get("/groups", auth, groupController.getList);
    server.get("/groups/:id", auth, groupController.getGroup);
    server.put("/groups/:id", auth, groupController.editGroup);
    server.post("/groups", auth, groupController.createGroup);
    server.del("/groups/:id", auth, groupController.removeGroup);

    // student routes
    server.get("/students", auth, studentController.getList);
    server.get("/students/:id", auth, studentController.getStudent);
    server.put("/students/:id", auth, studentController.editStudent);
    server.post("/students", auth, studentController.creatStudent);
    server.del("/students/:id", auth, studentController.removeStudent);

    // teacher routes
    server.get("/teachers", auth, teacherController.getList);
    server.get("/teachers/:id", auth, teacherController.getTeacher);
    server.put("/teachers/:id", auth, teacherController.editTeacher);
    server.post("/teachers", auth, teacherController.createTeacher);
    server.del("/teachers/:id", auth, teacherController.removeTeacher);

    // lesson routes
    server.get("/lessons", auth, lessonController.getList);
    server.get("/lessons/:id", auth, lessonController.getLesson);
    server.put("/lessons/:id", auth, lessonController.editLesson);
    server.post("/lessons", auth, lessonController.createLesson);
    server.del("/lessons/:id", auth, lessonController.removeLesson);
};
