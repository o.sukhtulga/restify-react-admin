const Group = require("../models").groups;
const ERRORS = require("../constants/ERROR");
const CustomError = require("../utils/error");

const { Op } = require("sequelize");

const create = async (data) => {
    const { name } = data;

    let group = await Group.findOne({
        where: {
            name: name,
        },
    });

    if (group) {
        throw new CustomError(ERRORS.GROUP_DUPLICATED);
    } else {
        group = await Group.create({
            name,
        });
    }

    return group;
};

const get = async (data) => {
    const { id } = data;

    let group = await Group.findOne({
        where: {
            id: id,
        },
    });

    if (!group) {
        throw new CustomError(ERRORS.GROUP_NOTFOUND);
    }

    return group;
};

const edit = async (data) => {
    const { id, name } = data;

    let group = await Group.findOne({
        where: {
            id: id,
        },
    });

    if (!group) {
        throw new CustomError(ERRORS.GROUP_NOTFOUND);
    }

    group.update({ name });

    return group;
};

const remove = async (data) => {
    const { id } = data;

    let group = await Group.findOne({
        where: {
            id: id,
        },
    });

    if (!group) {
        throw new CustomError(ERRORS.GROUP_NOTFOUND);
    }

    group.destroy({
        where: {
            id: id,
        },
    });

    return;
};

const list = async ({ filter, range, sort }) => {
    let options = {};
    if (Object.keys(filter).length === 0) {
        filter = JSON.parse(filter);
        options.where = {
            id: {
                [Op.in]: filter.id,
            },
        };
    }

    if (sort) {
        sort = JSON.parse(sort);
        options.order = [sort];
    }

    if (range) {
        range = JSON.parse(range);
        options.offset = range[0];
        options.limit = range[1] - range[0] + 1;
    }

    const { count, rows } = await Group.findAndCountAll(options);

    return {
        count,
        rows,
    };
};

module.exports = {
    create,
    list,
    get,
    edit,
    remove,
};
