const Lesson = require("../models").lessons;
const ERRORS = require("../constants/ERROR");
const CustomError = require("../utils/error");

const { Op, INTEGER } = require("sequelize");

const create = async (data) => {
    const { title, groups, startTime, endTime, room, teacher } = data;

    let lesson = await Lesson.findOne({
        where: {
            title: title,
        },
    });

    if (lesson) {
        throw new CustomError(ERRORS.LESSON_DUPLICATED);
    }

    lesson = await Lesson.create({
        title,
        groups: JSON.stringify(groups),
        startTime,
        endTime,
        room,
        teacher,
    });

    return lesson;
};

const get = async (data) => {
    const { id } = data;

    let lesson = await Lesson.findOne({
        where: {
            id: id,
        },
    });

    if (!lesson) {
        throw new CustomError(ERRORS.LESSON_NOTFOUND);
    }

    return {
        ...lesson.dataValues,
        groups: JSON.parse(lesson.dataValues.groups),
    };
};

const edit = async (data) => {
    const { id, title, groups, startTime, endTime, room, teacher } = data;

    let lesson = await Lesson.findOne({
        where: {
            id: id,
        },
    });

    if (!lesson) {
        throw new CustomError(ERRORS.LESSON_NOTFOUND);
    }

    lesson.update({ title, groups: JSON.stringify(groups), startTime, endTime, room, teacher });

    return lesson;
};

const remove = async (data) => {
    const { id } = data;

    let lesson = await Lesson.findOne({
        where: {
            id: id,
        },
    });

    if (!lesson) {
        throw new CustomError(ERRORS.LESSON_NOTFOUND);
    }

    lesson.destroy({
        where: {
            id: id,
        },
    });

    return;
};

const list = async ({ filter, range, sort }) => {
    let options = {};
    if (Object.keys(filter).length === 0) {
        filter = JSON.parse(filter);
        options.where = {
            id: {
                [Op.in]: filter.id,
            },
        };
    }

    if (sort) {
        sort = JSON.parse(sort);
        options.order = [sort];
    }

    if (range) {
        range = JSON.parse(range);
        options.offset = range[0];
        options.limit = range[1] - range[0] + 1;
    }

    const { count, rows } = await Lesson.findAndCountAll(options);

    const records = rows.map((item) => {
        return {
            ...item.dataValues,
            groups: JSON.parse(item.groups),
        };
    });

    return {
        count,
        rows: records,
    };
};

module.exports = {
    create,
    list,
    get,
    edit,
    remove,
};
