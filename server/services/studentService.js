const Student = require("../models").students;
const ERRORS = require("../constants/ERROR");
const CustomError = require("../utils/error");

const create = async (data) => {
    const { name, group } = data;

    let student = await Student.findOne({
        where: {
            name: name,
        },
    });

    if (student) {
        throw new CustomError(ERRORS.STUDENT_DUPLICATED);
    } else {
        student = await Student.create({
            name,
            group,
        });
    }

    return student;
};

const list = async ({ filter, range, sort }) => {
    let options = {};
    if (Object.keys(filter).length === 0) {
        filter = JSON.parse(filter);
        options.where = {
            id: {
                [Op.in]: filter.id,
            },
        };
    }

    if (sort) {
        sort = JSON.parse(sort);
        options.order = [sort];
    }

    if (range) {
        range = JSON.parse(range);
        options.offset = range[0];
        options.limit = range[1] - range[0] + 1;
    }

    const { count, rows } = await Student.findAndCountAll(options);

    return {
        count,
        rows,
    };
};

const get = async (data) => {
    const { id } = data;

    let student = await Student.findOne({
        where: {
            id: id,
        },
    });

    if (!student) {
        throw new CustomError(ERRORS.STUDENT_NOTFOUND);
    }

    return student;
};

const edit = async (data) => {
    const { id, name, group } = data;

    let student = await Student.findOne({
        where: {
            id: id,
        },
    });

    if (!student) {
        throw new CustomError(ERRORS.STUDENT_NOTFOUND);
    }

    student.update({ name, group });

    return student;
};

const remove = async (data) => {
    const { id } = data;

    let student = await Student.findOne({
        where: {
            id: id,
        },
    });

    if (!student) {
        throw new CustomError(ERRORS.STUDENT_NOTFOUND);
    }

    student.destroy({
        where: {
            id: id,
        },
    });

    return;
};

module.exports = {
    create,
    list,
    get,
    edit,
    remove
};
