const Teacher = require("../models").teachers;
const ERRORS = require("../constants/ERROR");
const CustomError = require("../utils/error");

const { Op } = require("sequelize");

const create = async (data) => {
    const { name } = data;

    let teacher = await Teacher.findOne({
        where: {
            name: name,
        },
    });

    if (teacher) {
        throw new CustomError(ERRORS.TEACHER_DUPLICATED);
    } else {
        teacher = await Teacher.create({
            name,
        });
    }

    return teacher;
};

const get = async (data) => {
    const { id } = data;

    let teacher = await Teacher.findOne({
        where: {
            id: id,
        },
    });

    if (!teacher) {
        throw new CustomError(ERRORS.TEACHER_NOTFOUND);
    }

    return teacher;
};

const edit = async (data) => {
    const { id, name } = data;

    let teacher = await Teacher.findOne({
        where: {
            id: id,
        },
    });

    if (!teacher) {
        throw new CustomError(ERRORS.TEACHER_NOTFOUND);
    }

    teacher.update({ name });

    return teacher;
};

const remove = async (data) => {
    const { id } = data;

    let teacher = await Teacher.findOne({
        where: {
            id: id,
        },
    });

    if (!teacher) {
        throw new CustomError(ERRORS.TEACHER_NOTFOUND);
    }

    teacher.destroy({
        where: {
            id: id,
        },
    });

    return;
};

const list = async ({ filter, range, sort }) => {
    let options = {};
    if (Object.keys(filter).length === 0) {
        filter = JSON.parse(filter);
        options.where = {
            id: {
                [Op.in]: filter.id,
            },
        };
    }

    if (sort) {
        sort = JSON.parse(sort);
        options.order = [sort];
    }

    if (range) {
        range = JSON.parse(range);
        options.offset = range[0];
        options.limit = range[1] - range[0] + 1;
    }

    const { count, rows } = await Teacher.findAndCountAll(options);

    return {
        count,
        rows,
    };
};

module.exports = {
    create,
    list,
    get,
    edit,
    remove,
};
