// const logger = require('../utils/logger');
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const User = require("../models").users;
const ERRORS = require("../constants/ERROR");
const CustomError = require("../utils/error");

const createUser = async (data) => {
    const { username, password } = data;

    let user = await User.findOne({
        where: {
            username: username,
        },
    });

    if (user) {
        throw new CustomError(ERRORS.USER_DUPLICATED);
    } else {
        encryptedPassword = await bcrypt.hash(password, 10);

        user = await User.create({
            username,
            password: encryptedPassword,
        });
    }

    return user;
};

const loginUser = async (data) => {
    const { username, password } = data;

    let user = await User.findOne({
        where: {
            username: username,
        },
    });

    if (!user) {
        throw new CustomError(ERRORS.USER_NOT_FOUND);
    }

    const isValidPassword = await bcrypt.compare(password, user.password);
    if (!isValidPassword) throw new CustomError(ERRORS.USERNAME_OR_PASSWORD_INCORRECT);

    const jwtToken = jwt.sign(
        {
            userid: user.id,
            username,
        },
        process.env.TOKEN_KEY,
        {
            expiresIn: "2h",
        }
    );

    return {
        token: jwtToken,
    };
};

module.exports = {
    createUser,
    loginUser,
};
