const { createLogger, format, transports } = require("winston");
require("winston-daily-rotate-file");
const { combine, printf } = format;
const moment = require("moment-timezone");

let path = require("path");
let PROJECT_ROOT = path.join(__dirname, "..");

const myFormat = printf((info) => {
    return `${info.timestamp} [${info.level}]: ${info.message}`;
});

const appendTimestamp = format((info, opts) => {
    if (opts.tz) info.timestamp = moment().tz(opts.tz).format();
    return info;
});

const logger = createLogger({
    level: "info",
    format: combine(appendTimestamp({ tz: "Asia/Ulaanbaatar" }), myFormat),
    //   defaultMeta: { service: "bank-statement-service" },
    transports: [
        //
        // - Write to all logs with level `info` and below to `combined.log`
        // - Write all logs error (and below) to `error.log`.
        //
        new transports.DailyRotateFile({
            level: "error",
            filename: "log/%DATE%-error.log",
            timestamp: true,
            maxSize: "10m",
            handleExceptions: true,
        }),
        new transports.DailyRotateFile({
            filename: "log/%DATE%-combined.log",
            timestamp: true,
            maxSize: "10m",
            handleExceptions: true,
        }),
    ],
    exitOnError: false,
});

if (process.env.NODE_ENV != "production") {
    logger.add(
        new transports.Console({
            level: "info",
            timestamp: true,
            handleExceptions: true,
        })
    );
}

module.exports.debug = module.exports.log = function () {
    logger.debug.apply(logger, formatLogArguments(arguments));
};

module.exports.info = function () {
    logger.info.apply(logger, formatLogArguments(arguments));
};

module.exports.warn = function () {
    logger.warn.apply(logger, formatLogArguments(arguments));
};

module.exports.error = function () {
    logger.error.apply(logger, formatLogArguments(arguments));
};

module.exports.stream = logger.stream;

/**
 * Attempts to add file and line number info to the given log arguments.
 */
function formatLogArguments(args) {
    args = Array.prototype.slice.call(args);

    var stackInfo = getStackInfo(1);

    if (stackInfo) {
        // get file path relative to project root
        var calleeStr = "(" + stackInfo.relativePath + ":" + stackInfo.line + ") ";

        if (typeof args[0] === "string") {
            args[0] = calleeStr + args[0];
        } else {
            args.unshift(calleeStr);
        }
    }

    return args;
}

/**
 * Parses and returns info about the call stack at the given index.
 */
function getStackInfo(stackIndex) {
    // get call stack, and analyze it
    // get all file, method, and line numbers
    var stacklist = new Error().stack.split("\n").slice(3);

    // stack trace format:
    // http://code.google.com/p/v8/wiki/JavaScriptStackTraceApi
    // do not remove the regex expresses to outside of this method (due to a BUG in node.js)
    var stackReg = /at\s+(.*)\s+\((.*):(\d*):(\d*)\)/gi;
    var stackReg2 = /at\s+()(.*):(\d*):(\d*)/gi;

    var s = stacklist[stackIndex] || stacklist[0];
    var sp = stackReg.exec(s) || stackReg2.exec(s);

    if (sp && sp.length === 5) {
        return {
            method: sp[1],
            relativePath: path.relative(PROJECT_ROOT, sp[2]),
            line: sp[3],
            pos: sp[4],
            file: path.basename(sp[2]),
            stack: stacklist.join("\n"),
        };
    }
}
