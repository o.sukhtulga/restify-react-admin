const CustomError = require("../utils/error");

const validator = async (schema, data) => {
    try {
        await schema.validateAsync(data);
    } catch (err) {
        throw new CustomError(err.message);
    }
};

module.exports = validator;
